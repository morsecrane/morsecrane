/******************************************************************************
 *  This file is part of MorseCrane.                                          *
 *                                                                            *
 *  Copyright (C) 2011  Rouven Spreckels  <nevuor@nevux.org>                  *
 *                                                                            *
 *  MorseCrane is free software: you can redistribute it and/or modify        *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  MorseCrane is distributed in the hope that it will be useful,             *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with MorseCrane.  If not, see <http://www.gnu.org/licenses/>.       *
 ******************************************************************************/

class Average {
	private double value[];
	private int index = 0;
	private boolean filled = false;
	public Average(int size) {
		value = new double[size];
	}
	public void set(double value) {
		this.value[index] = value;
		if (++index == this.value.length) {
			index = 0;
			filled = true;
		}
	}
	public double get() throws Exception {
		int length = filled ? value.length : index;
		if (length == 0)
			throw new Exception("No values");
		double sum = 0;
		for (int i = 0; i < length; ++i)
			sum += value[i];
		return sum/length;
	}
	public void clear() {
		filled = false;
		index = 0;
	}
}
