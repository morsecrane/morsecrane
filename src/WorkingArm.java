/******************************************************************************
 *  This file is part of MorseCrane.                                          *
 *                                                                            *
 *  Copyright (C) 2011  Rouven Spreckels  <nevuor@nevux.org>                  *
 *                                                                            *
 *  MorseCrane is free software: you can redistribute it and/or modify        *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  MorseCrane is distributed in the hope that it will be useful,             *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with MorseCrane.  If not, see <http://www.gnu.org/licenses/>.       *
 ******************************************************************************/

import lejos.nxt.Motor;

class WorkingArm extends Machine {
	WorkingArm(double initial, double minimum, double maximum, double scale, double scope, int moves) {
		super(initial, minimum, maximum, scale, scope, moves);
	}
	WorkingArm(double initial, double minimum, double maximum, double scale, double scope, int moves, int speed, int acceleration) {
		super(initial, minimum, maximum, scale, scope, moves, speed, acceleration);
	}
	protected void move(double degree) {
		Motor.A.rotate((int)Math.round(degree), true);
	}
	boolean isMoving() {
		return Motor.A.isMoving();
	}
	boolean isStalled() {
		return Motor.A.isStalled();
	}
	void release() {
		Motor.A.flt();
	}
	void stop() {
		Motor.A.stop();
	}
	void setSpeed(int speed) {
		Motor.A.setSpeed(speed);
	}
	void setAcceleration(int acceleration) {
		Motor.A.setAcceleration(acceleration);
	}
}
