/******************************************************************************
 *  This file is part of MorseCrane.                                          *
 *                                                                            *
 *  Copyright (C) 2011  Rouven Spreckels  <nevuor@nevux.org>                  *
 *                                                                            *
 *  MorseCrane is free software: you can redistribute it and/or modify        *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  MorseCrane is distributed in the hope that it will be useful,             *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with MorseCrane.  If not, see <http://www.gnu.org/licenses/>.       *
 ******************************************************************************/

public abstract class MorseCode extends Signal {
	private static final int WORD = 10;
	private static final byte
	DIT = 0,
	DAH = 1;
	private static byte DI(byte inner) {
		return (byte)(inner << 1 | DIT);
	}
	private static byte DA(byte inner) {
		return (byte)(inner << 1 | DAH);
	}
	private static final byte
	T = 1,
	H = 1;
	private static byte DAH(byte inner) {
		return DA(inner);
	}
	private static class Sign {
		char sign;
		byte code;
		Sign(char sign, byte code) {
			this.sign = sign;
			this.code = code;
		}
	};
	public MorseCode(int unitPeriode, int samples, double treshold) {
		super(unitPeriode, samples, treshold);
		clear();
	}
	private static final Sign[] sign = {
		new Sign('a', DI(DA(H))),
		new Sign('b', DAH(DI(DI(DI(T))))),
		new Sign('c', DAH(DI(DAH(DI(T))))),
		new Sign('d', DAH(DI(DI(T)))),
		new Sign('e', DI(T)),
		new Sign('f', DI(DI(DAH(DI(T))))),
		new Sign('g', DAH(DAH(DI(T)))),
		new Sign('h', DI(DI(DI(DI(T))))),
		new Sign('i', DI(DI(T))),
		new Sign('j', DI(DAH(DAH(DA(H))))),
		new Sign('k', DAH(DI(DA(H)))),
		new Sign('l', DI(DAH(DI(DI(T))))),
		new Sign('m', DAH(DA(H))),
		new Sign('n', DAH(DI(T))),
		new Sign('o', DAH(DAH(DA(H)))),
		new Sign('p', DI(DAH(DAH(DI(T))))),
		new Sign('q', DAH(DAH(DI(DA(H))))),
		new Sign('r', DI(DAH(DI(T)))),
		new Sign('s', DI(DI(DI(T)))),
		new Sign('t', DA(H)),
		new Sign('u', DI(DI(DA(H)))),
		new Sign('v', DI(DI(DI(DA(H))))),
		new Sign('w', DI(DAH(DA(H)))),
		new Sign('x', DAH(DI(DI(DA(H))))),
		new Sign('y', DAH(DI(DAH(DA(H))))),
		new Sign('z', DAH(DAH(DI(DI(T))))),
		new Sign('0', DAH(DAH(DAH(DAH(DA(H)))))),
		new Sign('1', DI(DAH(DAH(DAH(DA(H)))))),
		new Sign('2', DI(DI(DAH(DAH(DA(H)))))),
		new Sign('3', DI(DI(DI(DAH(DA(H)))))),
		new Sign('4', DI(DI(DI(DI(DA(H)))))),
		new Sign('5', DI(DI(DI(DI(DI(T)))))),
		new Sign('6', DAH(DI(DI(DI(DI(T)))))),
		new Sign('7', DAH(DAH(DI(DI(DI(T)))))),
		new Sign('8', DAH(DAH(DAH(DI(DI(T)))))),
		new Sign('9', DAH(DAH(DAH(DAH(DI(T)))))),
		new Sign('.', DI(DAH(DI(DAH(DI(DA(H))))))),
		new Sign(',', DAH(DAH(DI(DI(DAH(DA(H))))))),
		new Sign('?', DI(DI(DAH(DAH(DI(DI(T))))))),
		new Sign('\'',DI(DAH(DAH(DAH(DAH(DI(T))))))),
		new Sign('!', DAH(DI(DAH(DI(DAH(DA(H))))))),
		new Sign('\\',DAH(DI(DI(DAH(DI(T)))))),
		new Sign('(', DAH(DI(DAH(DAH(DI(T)))))),
		new Sign(')', DAH(DI(DAH(DAH(DI(DA(H))))))),
		new Sign('&', DI(DAH(DI(DI(DI(T)))))),
		new Sign(':', DAH(DAH(DAH(DI(DI(DI(T))))))),
		new Sign(';', DAH(DI(DAH(DI(DAH(DI(T))))))),
		new Sign('=', DAH(DI(DI(DI(DA(H)))))),
		new Sign('+', DI(DAH(DI(DAH(DI(T)))))),
		new Sign('-', DAH(DI(DI(DI(DI(DA(H))))))),
		new Sign('_', DI(DI(DAH(DAH(DI(DA(H))))))),
		new Sign('"', DI(DAH(DI(DI(DAH(DI(T))))))),
		new Sign('$', DI(DI(DI(DAH(DI(DI(DA(H)))))))),
		new Sign('@', DI(DAH(DAH(DI(DAH(DI(T))))))),
	};
	private static class Prosign {
		String sign;
		short code;
		Prosign(String sign, byte code) {
			this.sign = sign;
			this.code = code;
		}
	};
	private static final Prosign[] prosign = {
		new Prosign("ar", DI(DAH(DI(DAH(DI(T)))))),
		new Prosign("as", DI(DAH(DI(DI(DI(T)))))),
		new Prosign("bk", DAH(DI(DI(DI(DAH(DI(DA(H)))))))),
		new Prosign("bt", DAH(DI(DI(DI(DA(H)))))),
		new Prosign("cl", DAH(DI(DAH(DI(DI(DAH(DI(DI(T))))))))),
		new Prosign("ct", DAH(DI(DAH(DI(DA(H)))))),
		new Prosign("do", DAH(DI(DI(DAH(DAH(DA(H))))))),
		new Prosign("kn", DAH(DI(DAH(DAH(DI(T)))))),
		new Prosign("sk", DI(DI(DI(DAH(DI(DA(H))))))),
		new Prosign("sn", DI(DI(DI(DAH(DI(T)))))),
		new Prosign("sos",DI(DI(DI(DAH(DAH(DAH(DI(DI(DI(T)))))))))),
	};
	private static byte signal;
	private static byte code;
	private static short didas;
	private static boolean signed;
	private static boolean word;
	private void clear() {
		signal = 1;
		didas = 0;
		code = 0;
		signed = false;
		word = false;
	}
	public void onUnit(boolean value) throws Exception {
		System.out.print(value ? 1 : 0);
		signal = (byte)(signal << 1 | (value ? 1 : 0));
		switch (signal) {
			case 2:
			case 3:
			break;
			case 4:
				if (!signed) {
					signed = true;
					code |= 1 << didas;
					signal = 1;
					for (int i = 0; i < sign.length; ++i)
						if (sign[i].code == code) {
							onSign(""+sign[i].sign);
							code = 0;
							didas = 0;
							return;
						}
					for (int i = 0; i < prosign.length; ++i)
						if (prosign[i].code == code) {
							onProsign(prosign[i].sign);
							code = 0;
							didas = 0;
							return;
						}
					if (didas >= 6) {
						if ((code & ~(1 << didas)) == 0)
							correction();
						else
						if (didas > 9) {
							String error = "Too long code: "+code;
							clear();
							throw new Exception(error);
						}
					}
					String error = "Invalid code: "+code;
					clear();
					throw new Exception(error);
				}
			break;
			case 6:
				code |= DIT << didas++;
				signal = 1;
				signed = false;
			break;
			case 7:
			break;
			case 8:
				if (word) {
					signed = false;
					signal = 1;
					word = false;
					onSign(" ");
				}
			break;
			case 15:
			break;
			case 16:
				signal = 1;
				if (WORD == 7) {
					signed = false;
					onSign(" ");
				}
				else
					word = true;
			break;
			case 30:
				code |= DAH << didas++;
				signal = 1;
				signed = false;
			break;
			default:
				clear();
				throw new Exception("Invalid signal: "+signal);
		}
	}
	protected abstract void onSign(String sign) throws Exception;
	protected abstract void onProsign(String prosign) throws Exception;
	protected abstract void correction() throws Exception;
}
