/******************************************************************************
 *  This file is part of MorseCrane.                                          *
 *                                                                            *
 *  Copyright (C) 2011  Rouven Spreckels  <nevuor@nevux.org>                  *
 *                                                                            *
 *  MorseCrane is free software: you can redistribute it and/or modify        *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  MorseCrane is distributed in the hope that it will be useful,             *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with MorseCrane.  If not, see <http://www.gnu.org/licenses/>.       *
 ******************************************************************************/

public class E6410 extends Keyboard {
	private static final Position offset = new Position(-0.1275, 0);
	private static final Key[] MAP = {
		new Key("q", 0.038, 0.0875),
		new Key("w", 0.056, 0.0875),
		new Key("e", 0.076, 0.0875),
		new Key("r", 0.095, 0.0875),
		new Key("t", 0.114, 0.0875),
		new Key("z", 0.134, 0.0875),
		new Key("u", 0.152, 0.0875),
		new Key("i", 0.171, 0.0875),
		new Key("o", 0.190, 0.0875),
		new Key("p", 0.209, 0.0875),
		new Key("ü", 0.227, 0.0875),
		new Key("+", 0.246, 0.0875),
		new Key("a", 0.041, 0.0675),
		new Key("s", 0.062, 0.0675),
		new Key("d", 0.081, 0.0675),
		new Key("f", 0.010, 0.0675),
		new Key("g", 0.118, 0.0675),
		new Key("h", 0.136, 0.0675),
		new Key("j", 0.155, 0.0675),
		new Key("k", 0.176, 0.0675),
		new Key("l", 0.195, 0.0675),
		new Key("ö", 0.218, 0.0675),
		new Key("ä", 0.232, 0.0675),
		new Key("#", 0.251, 0.0675),
		new Key("<", 0.032, 0.049),
		new Key("y", 0.058, 0.049),
		new Key("x", 0.072, 0.049),
		new Key("c", 0.090, 0.049),
		new Key("v", 0.110, 0.049),
		new Key("b", 0.129, 0.049),
		new Key("n", 0.147, 0.049),
		new Key("m", 0.166, 0.049),
		new Key(",", 0.185, 0.049),
		new Key(".", 0.205, 0.049),
		new Key("-", 0.224, 0.049),
		new Key("^", 0.010, 0.105),
		new Key("1", 0.028, 0.105),
		new Key("2", 0.047, 0.105),
		new Key("3", 0.066, 0.105),
		new Key("4", 0.086, 0.105),
		new Key("5", 0.105, 0.105),
		new Key("6", 0.124, 0.105),
		new Key("7", 0.142, 0.105),
		new Key("8", 0.161, 0.105),
		new Key("9", 0.181, 0.105),
		new Key("0", 0.199, 0.105),
		new Key("ß", 0.219, 0.105),
		new Key("´", 0.238, 0.105),
		new Key("rstrg", 0.216, 0.028),
		new Key("altgr", 0.195, 0.028),
		new Key("space", 0.138, 0.028),
		new Key("alt", 0.078, 0.028),
		new Key("win", 0.057, 0.028),
		new Key("fn", 0.037, 0.028),
		new Key("lstrg", 0.015, 0.028),
		new Key("lshift", 0.013, 0.049),
		new Key("caps", 0.016, 0.0675),
		new Key("tab", 0.016, 0.0875),
		new Key("bspace", 0.267, 0.105),
		new Key("return", 0.272, 0.008),
		new Key("rshift", 0.262, 0.049),
		new Key("up", 0.258, 0.031),
		new Key("left", 0.240, 0.016),
		new Key("down", 0.258, 0.016),
		new Key("right", 0.277, 0.016),
		new Key("esc", 0.010, 0.1225),
		new Key("f1", 0.030, 0.1225),
		new Key("f2", 0.046, 0.1225),
		new Key("f3", 0.065, 0.1225),
		new Key("f4", 0.083, 0.1225),
		new Key("f5", 0.100, 0.1225),
		new Key("f6", 0.118, 0.1225),
		new Key("f7", 0.135, 0.1225),
		new Key("f8", 0.152, 0.1225),
		new Key("f9", 0.172, 0.1225),
		new Key("f10", 0.188, 0.1225),
		new Key("f11", 0.205, 0.1225),
		new Key("f12", 0.223, 0.1225),
		new Key("entf", 0.242, 0.1225),
		new Key("ende", 0.260, 0.1225),
		new Key("pagedown", 0.276, 0.1225),
		new Key("einfg", 0.242, 0.1325),
		new Key("pos1", 0.260, 0.1325),
		new Key("pageup", 0.276, 0.1325),
		new Key("lmouse", 0.110, 0.010),
		new Key("mmouse", 0.130, 0.010),
		new Key("rmouse", 0.150, 0.010),
	};
	public E6410(Position position) {
		super(Position.add(position, offset));
	}
	public Key get(String key) throws Exception {
		return new Key(get(MAP, key));
	}
	public Keyboard polar() {
		return polar(MAP);
	}
}
