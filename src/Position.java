/******************************************************************************
 *  This file is part of MorseCrane.                                          *
 *                                                                            *
 *  Copyright (C) 2011  Rouven Spreckels  <nevuor@nevux.org>                  *
 *                                                                            *
 *  MorseCrane is free software: you can redistribute it and/or modify        *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  MorseCrane is distributed in the hope that it will be useful,             *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with MorseCrane.  If not, see <http://www.gnu.org/licenses/>.       *
 ******************************************************************************/

class Position {
	protected double[] position;
	public Position(Position position) {
		set(position);
	}
	public Position(double a, double b) {
		position = new double[] {a, b};
	}
	public Position set(Position position) {
		this.position = position.get();
		return this;
	}
	public static Position add(Position a, Position b) {
		return new Position(a.get()[0]+b.get()[0], a.get()[1]+b.get()[1]);
	}
	public Position add(Position position) {
		return set(add(this, position));
	}
	public double[] get() {
		return position;
	}
	private static double hypot(double a, double b) {
		return Math.sqrt(Math.pow(a, 2)+Math.pow(b, 2));
	}
	public static Position polar(Position position) {
		return new Position(hypot(position.get()[0], position.get()[1]), Math.atan2(position.get()[1], position.get()[0]));
	}
	public Position polar() {
		return set(polar(this));
	}
	public String toString() {
		String string = "("+position[0];
		for (int i = 1; i < position.length; ++i) {
			string = string+", "+position[i];
		}
		return string+")";
	}
}
