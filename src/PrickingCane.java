/******************************************************************************
 *  This file is part of MorseCrane.                                          *
 *                                                                            *
 *  Copyright (C) 2011  Rouven Spreckels  <nevuor@nevux.org>                  *
 *                                                                            *
 *  MorseCrane is free software: you can redistribute it and/or modify        *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  MorseCrane is distributed in the hope that it will be useful,             *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with MorseCrane.  If not, see <http://www.gnu.org/licenses/>.       *
 ******************************************************************************/

import lejos.nxt.Motor;

class PrickingCane extends Machine {
	PrickingCane(double initial, double minimum, double maximum, double scale, double scope, int moves) {
		super(initial, minimum, maximum, scale, scope, moves);
	}
	PrickingCane(double initial, double minimum, double maximum, double scale, double scope, int moves, int speed, int acceleration) {
		super(initial, minimum, maximum, scale, scope, moves, speed, acceleration);
	}
	protected void move(double degree) {
		Motor.B.rotate((int)Math.round(degree), true);
	}
	boolean isMoving() {
		return Motor.B.isMoving();
	}
	boolean isStalled() {
		return Motor.B.isStalled();
	}
	void release() {
		Motor.B.flt();
	}
	void stop() {
		Motor.B.stop();
	}
	void setSpeed(int speed) {
		Motor.B.setSpeed(speed);
	}
	void setAcceleration(int acceleration) {
		Motor.B.setAcceleration(acceleration);
	}
}
