/******************************************************************************
 *  This file is part of MorseCrane.                                          *
 *                                                                            *
 *  Copyright (C) 2011  Rouven Spreckels  <nevuor@nevux.org>                  *
 *                                                                            *
 *  MorseCrane is free software: you can redistribute it and/or modify        *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  MorseCrane is distributed in the hope that it will be useful,             *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with MorseCrane.  If not, see <http://www.gnu.org/licenses/>.       *
 ******************************************************************************/

public class Crane {
	private Machine[] machine;
	private static enum Status {
		POSITIONING,
		PRESSING,
		RELEASING,
		RESETTING,
		DONE
	}
	private Status status = Status.DONE;
	public Crane(Machine workingArm, Machine slewingUnit, Machine prickingCane) {
		machine = new Machine[] {workingArm, slewingUnit, prickingCane};
	}
	public void schedule() throws Exception {
		switch (status) {
			case POSITIONING:
				if (machine[0].done() && machine[1].done()) {
					status = Status.PRESSING;
					machine[2].moveTo(0);
				}
			break;
			case PRESSING:
				if (machine[2].done()) {
					status = Status.RELEASING;
					machine[2].moveTo(machine[2].getInitialPosition());
				}
			break;
			case RELEASING:
				if (machine[2].done()) {
					status = Status.DONE;
				}
			break;
			case RESETTING:
					if (machine[0].done() && machine[1].done() && machine[2].done())
						status = Status.DONE;
			break;
		}
		for (int i = 0; i < machine.length; ++i)
			machine[i].schedule();
	}
	public void reset() throws Exception {
		if (!done())
			throw new Exception("Undone");
		status = Status.RESETTING;
		for (int i = 0; i < machine.length; ++i)
			machine[i].reset();
	}
	public void moveTo(Position position) throws Exception {
		if (!done())
			throw new Exception("Undone");
		status = Status.POSITIONING;
		machine[0].moveTo(position.get()[0]);
		machine[1].moveTo(position.get()[1]);
	}
	public boolean done() {
		return (status == Status.DONE);
	}
}
