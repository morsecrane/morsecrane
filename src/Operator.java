/******************************************************************************
 *  This file is part of MorseCrane.                                          *
 *                                                                            *
 *  Copyright (C) 2011  Rouven Spreckels  <nevuor@nevux.org>                  *
 *                                                                            *
 *  MorseCrane is free software: you can redistribute it and/or modify        *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  MorseCrane is distributed in the hope that it will be useful,             *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with MorseCrane.  If not, see <http://www.gnu.org/licenses/>.       *
 ******************************************************************************/

public class Operator {
	private Crane crane;
	private Keyboard keyboard;
	private boolean inSign = false;
	private boolean capsLock = false;
	private boolean inTime;
	private boolean flushing;
	private String keys = "";
	public Operator(Crane crane, Keyboard keyboard) {
		this.crane = crane;
		this.keyboard = keyboard;
		this.inTime = flushing = true;
	}
	public Operator(Crane crane, Keyboard keyboard, boolean inTime) {
		this.crane = crane;
		this.keyboard = keyboard;
		this.inTime = flushing = inTime;
	}
	public String shift(String s, int i) {
		return i > s.length() ? "" : s.substring(i, s.length());
	}
	public void schedule() throws Exception {
		if (keys.length() > 0 && flushing && crane.done()) {
			if (inSign) {
				int i = keys.indexOf(")");
				if (i >= 0) {
					String key = keys.substring(0, i);
					inSign = false;
					crane.moveTo(keyboard.get(key));
					keys = shift(keys, i+1);
				}
			}
			else {
				String key = keys.substring(0, 1);
				if (key.equals("(")) {
					inSign = true;
					keys = shift(keys, 1);
				}
				else
				if (key.equals(key.toLowerCase()) == capsLock) {
					crane.moveTo(keyboard.get("caps"));
					capsLock = !(capsLock);
				}
				else {
					crane.moveTo(keyboard.get(key.toLowerCase()));
					keys = shift(keys, 1);
				}
			}
		}
		else
		if (!inTime && done())
			flushing = false;
		crane.schedule();
	}
	public void clear() {
		keys = "";
		flushing = inTime;
	}
	public void reset() throws Exception {
		clear();
		crane.reset();
	}
	public void drive(String keys) throws Exception {
		if (!inTime && flushing)
			throw new Exception("Undone");
		this.keys += keys;
	}
	public void flush() {
		flushing = true;
	}
	public boolean done() {
		return (keys.length() == 0 && crane.done());
	}
}
