/******************************************************************************
 *  This file is part of MorseCrane.                                          *
 *                                                                            *
 *  Copyright (C) 2011  Rouven Spreckels  <nevuor@nevux.org>                  *
 *                                                                            *
 *  MorseCrane is free software: you can redistribute it and/or modify        *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  MorseCrane is distributed in the hope that it will be useful,             *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with MorseCrane.  If not, see <http://www.gnu.org/licenses/>.       *
 ******************************************************************************/

import lejos.nxt.*;

public class MorseCrane extends MorseSound {
	private static enum Status {
		FLUSHING,
		RESETTING,
		DONE
	}
	private Status status = Status.DONE;
	private Operator operator;
	public MorseCrane(Operator operator, int unitPeriode, int samples, double treshold) {
		super(unitPeriode, samples, treshold);
		this.operator = operator;
	}
	public void clear() {
		operator.clear();
	}
	public void reset() throws Exception {
		operator.reset();
	}
	public void listening(boolean listening) {
		if (listening)
			System.out.println("\nListening..");
		else
			System.out.println("\nWaiting..");
		super.listening(listening);
	}
	public boolean done() {
		return operator.done() && status == Status.DONE;
	}
	protected void onSign(String sign) throws Exception {
		if (sign.equals(" "))
			sign = "(space)";
		System.out.print(":"+sign+";");
		operator.drive(sign);
	}
	protected void onProsign(String prosign) throws Exception {
		System.out.print(":["+prosign+"];");
		if (prosign.equals("sk")) {
			listening(false);
			operator.flush();
			status = Status.FLUSHING;
		}
	}
	protected void correction() throws Exception {
		operator.drive("bspace");
	}
	public void schedule() throws Exception {
		switch (status) {
			case FLUSHING:
				if (operator.done()) {
					status = Status.RESETTING;
					operator.reset();
				}
			break;
			case RESETTING:
				if (operator.done()) {
					status = Status.DONE;
					listening(true);
				}
			break;
		}
		super.schedule();
		operator.schedule();
	}
	public static void main(String[] args) {
		MorseCrane morseCrane = new MorseCrane(
			new Operator(
				new Crane(
					new WorkingArm(0.165, 0.165, 0.395, 7200, 3, 50),
					new SlewingUnit(Math.PI/2, 0, Math.PI, -1400, -285, 50),
					new PrickingCane(0.035, 0, 0.035, -3316, -5, 50)
				),
				new E6410(new Position(0, 0.20)).polar(),
				true
			),
			1000,
			10,
			0.45
		);
		morseCrane.listening(true);
		boolean quit = false;
		boolean reset = false;
		boolean error = false;
		while (!(quit && !reset && morseCrane.done())) {
			try {
				if (!error) {
					if (reset) {
						if (morseCrane.done()) {
							morseCrane.reset();
							reset = false;
						}
					}
					else
					if (Button.ENTER.isPressed() && !quit) {
						Thread.sleep(200);
						quit = true;
						reset = true;
						morseCrane.listening(false);
					}
				}
				else {
					if (reset) {
						if (morseCrane.done()) {
							morseCrane.reset();
							reset = false;
						}
					}
					else {
						if (Button.ENTER.isPressed() && morseCrane.done()) {
							Thread.sleep(200);
							error = false;
							morseCrane.listening(true);
						}
					}
				}
				morseCrane.schedule();
				Thread.sleep(morseCrane.getTimeBase());
			} catch (Exception e) {
				error = true;
				reset = true;
				System.out.println("\n"+e.getMessage());
				morseCrane.listening(false);
				morseCrane.clear();
			}
		}
	}
}
