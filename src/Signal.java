/******************************************************************************
 *  This file is part of MorseCrane.                                          *
 *                                                                            *
 *  Copyright (C) 2011  Rouven Spreckels  <nevuor@nevux.org>                  *
 *                                                                            *
 *  MorseCrane is free software: you can redistribute it and/or modify        *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  MorseCrane is distributed in the hope that it will be useful,             *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with MorseCrane.  If not, see <http://www.gnu.org/licenses/>.       *
 ******************************************************************************/

public abstract class Signal {
	private double treshold;
	private int sample = 0, samples;
	private boolean record = false, listening = false;
	private int unitPeriode;
	private int samplePeriode;
	private Average value;
	public Signal(int unitPeriode, int samples, double treshold) {
		this.unitPeriode = unitPeriode;
		this.treshold = treshold;
		this.samples = samples;
		samplePeriode = unitPeriode/samples;
		value = new Average(samples);
	}
	public int getUnitPeriode() {
		return unitPeriode;
	}
	public int getTimeBase() {
		return samplePeriode;
	}
	public void listening(boolean listening) {
		this.listening = listening;
		value.clear();
		this.record = false;
		sample = 0;
	}
	public void schedule() throws Exception {
		if (listening) {
			if (record) {
				if (++sample < samples)
					value.set(value());
				else {
					sample = 0;
					onUnit(value.get() > treshold);
				}
			}
			else {
				value.set(value());
				if (++sample == samples) {
					sample = 0;
					if (value.get() > treshold) {
						value.clear();
						record = true;
						onUnit(true);
					}
				}
			}
		}
	}
	public abstract double value();
	public abstract void onUnit(boolean value) throws Exception;
}
