/******************************************************************************
 *  This file is part of MorseCrane.                                          *
 *                                                                            *
 *  Copyright (C) 2011  Rouven Spreckels  <nevuor@nevux.org>                  *
 *                                                                            *
 *  MorseCrane is free software: you can redistribute it and/or modify        *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  MorseCrane is distributed in the hope that it will be useful,             *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with MorseCrane.  If not, see <http://www.gnu.org/licenses/>.       *
 ******************************************************************************/

import lejos.nxt.Motor;

class SlewingUnit extends Machine {
	SlewingUnit(double initial, double minimum, double maximum, double scale, double scope, int moves) {
		super(initial, minimum, maximum, scale, scope, moves);
	}
	SlewingUnit(double initial, double minimum, double maximum, double scale, double scope, int moves, int speed, int acceleration) {
		super(initial, minimum, maximum, scale, scope, moves, speed, acceleration);
	}
	protected void move(double degree) {
		Motor.C.rotate((int)Math.round(degree), true);
	}
	boolean isMoving() {
		return Motor.C.isMoving();
	}
	boolean isStalled() {
		return Motor.C.isStalled();
	}
	void release() {
		Motor.C.flt();
	}
	void stop() {
		Motor.C.stop();
	}
	void setSpeed(int speed) {
		Motor.C.setSpeed(speed);
	}
	void setAcceleration(int acceleration) {
		Motor.C.setAcceleration(acceleration);
	}
}
