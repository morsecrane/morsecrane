/******************************************************************************
 *  This file is part of MorseCrane.                                          *
 *                                                                            *
 *  Copyright (C) 2011  Rouven Spreckels  <nevuor@nevux.org>                  *
 *                                                                            *
 *  MorseCrane is free software: you can redistribute it and/or modify        *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  MorseCrane is distributed in the hope that it will be useful,             *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with MorseCrane.  If not, see <http://www.gnu.org/licenses/>.       *
 ******************************************************************************/

abstract class Machine {
	private static enum Status {
		MOVING,
		RESETTING,
		DONE
	}
	private Status status = Status.DONE;
	private static final double EPSILON = 0.001;
	private final double initial;
	private final double minimum;
	private final double maximum;
	private double current;
	private final double scale;
	private final double scope;
	private final int moves;
	private int direction = 1;
	private int move;
	Machine(double initial, double minimum, double maximum, double scale, double scope, int moves) {
		this.initial = current = initial;
		this.minimum = minimum;
		this.maximum = maximum;
		this.scale = scale;
		this.scope = scope;
		this.moves = moves;
	}
	Machine(double initial, double minimum, double maximum, double scale, double scope, int moves, int speed, int acceleration) {
		this.initial = current = initial;
		this.minimum = minimum;
		this.maximum = maximum;
		this.scale = scale;
		this.scope = scope;
		this.moves = moves;
		setSpeed(speed);
		setAcceleration(acceleration);
	}
	void calibrate() {
	}
	private static boolean equals(double a, double b) {
		return a == b ? true : Math.abs(a-b) < EPSILON;
	}
	public boolean inRange(double position) {
		return (position >= minimum && position <= maximum);
	}
	public void reset() throws Exception {
		moveTo(initial);
		if (direction != 1) {
			status = Status.RESETTING;
			direction = 1;
		}
	}
	void schedule() {
		switch (status) {
			case MOVING:
				if (!isMoving()) {
					release();
					status = Status.DONE;
				}
			break;
			case RESETTING:
				if (!isMoving()) {
					move(scope);
					release();
					status = Status.DONE;
				}
			break;
		}
	}
	void moveTo(double position) throws Exception {
		if (!done())
			throw new Exception("Undone");
		if (!inRange(position))
			throw new Exception("Out of range: "+position+" ("+minimum+","+maximum+")");
		status = Status.MOVING;
		double difference = position-current;
		if (equals(difference, 0))
			return;
		double change = 0;
		current = position;
		if (direction != Math.signum(difference)) {
			direction = -direction;
			change = direction*scope;
		}
		move(scale*difference+change);
		if (++move == moves) {
			move = 0;
			calibrate();
		}
	}
	public boolean done() {
		return !isMoving() && status == Status.DONE;
	}
	protected abstract void move(double degree);
	abstract boolean isMoving();
	abstract boolean isStalled();
	abstract void release();
	abstract void stop();
	void setPosition(double position) {
		current = position;
	}
	public double getInitialPosition() {
		return initial;
	}
	abstract void setSpeed(int speed);
	abstract void setAcceleration(int acceleration);
}
