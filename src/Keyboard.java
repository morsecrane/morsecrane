/******************************************************************************
 *  This file is part of MorseCrane.                                          *
 *                                                                            *
 *  Copyright (C) 2011  Rouven Spreckels  <nevuor@nevux.org>                  *
 *                                                                            *
 *  MorseCrane is free software: you can redistribute it and/or modify        *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  MorseCrane is distributed in the hope that it will be useful,             *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with MorseCrane.  If not, see <http://www.gnu.org/licenses/>.       *
 ******************************************************************************/

abstract class Keyboard {
	private final Position position;
	public Keyboard(Position position) {
		this.position = position;
	}
	protected static Key get(Key[] map, String key) throws Exception {
		for (int i = 0; i < map.length; ++i)
			if (map[i].getName().equals(key))
				return map[i];
		throw new Exception("No '"+key+"' key");
	}
	protected Keyboard polar(Key[] map) {
		for (int i = 0; i < map.length; ++i)
			map[i].add(position).polar();
		return this;
	}
	public abstract Key get(String key) throws Exception;
	public abstract Keyboard polar();
}
